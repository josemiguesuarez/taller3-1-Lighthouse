'use strict';

const Gatherer = require('lighthouse').Gatherer;

class ApiRequest extends Gatherer {
    afterPass(options) {
        const driver = options.driver;

        return driver.evaluateAsync('window.apiRequestTime')
            .then(apiRequestTime => {
                if (!apiRequestTime) {

                    throw new Error('Unable to find card load metrics in page');
                }
                return apiRequestTime;
            });
    }
}

module.exports = ApiRequest;