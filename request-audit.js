'use strict';

const Audit = require('lighthouse').Audit;

const MAX_REQ_TIME = 3000;

class LoadAudit extends Audit {
    static get meta() {
        return {
            category: 'MyPerformance',
            name: 'request-audit',
            description: 'Petición inferior a 3 segundos',
            failureDescription: 'Petición mayor a 3 segundo :(',
            helpText: 'Se usa para determinar si el tiempo de respuesta de una petición es adecudado.',

            requiredArtifacts: ['ApiRequest']
        };
    }

    static audit(artifacts) {
        const loadedTime = artifacts.ApiRequest;

        const belowThreshold = loadedTime <= MAX_REQ_TIME;

        return {
            rawValue: loadedTime,
            score: belowThreshold
        };
    }
}

module.exports = LoadAudit;